﻿using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Security;

namespace Mail_CSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            var ServHote = "smtp.office365.com";
            var Port = 587;
            var User = "notifications@bmp-progelink.com";
            var Pass = "Woq34768";
            var MailTo = "arnaud.frossard@bmp-progelink.com";
            var MailFrom = "notifications@bmp-progelink.com";
            var Subject = "Challenge #2 test html";
            var Body = Path.Combine(Environment.CurrentDirectory + "Html/index.htm");

            MailAddress from = new MailAddress(MailFrom);
            MailAddress to = new MailAddress(MailTo);

            MailMessage message = new MailMessage(from, to);
            message.Subject = Subject;
            message.IsBodyHtml = true;
            message.Body = Body;

            SmtpClient client = new SmtpClient();
            client.Host = ServHote;
            client.Port = Port;
            client.EnableSsl = true;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(User, Pass);

            try
            {
                client.Send(message);
                Console.WriteLine("envoi d'un email a {0} en utilisant smtp host {1} et le port {2}", to.ToString(), client.Host, client.Port);
            }
            catch (Exception e)
            {
                Console.WriteLine("{0}", e.StackTrace);
                Console.WriteLine("error");
            }

            
            

        }
    }
}
